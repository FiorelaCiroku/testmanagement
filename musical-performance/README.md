# Musical Performance
The Musical Performance ontology module of the [Polifonia ontology network](https://github.com/polifonia-project/ON) models concepts and relationships for representing musical performances and music-related events. 

## Competency questions addressed by this ontology module 

**From the [Carolina - Sources Cross Analysis](https://github.com/polifonia-project/stories/blob/main/Carolina:%20Music%20Historian/Carolina%20-%20Sources%20cross%20analysis.md) story**
- Where was a musical composition performed?
  -  In which buildings was a musical composition performed?
- Where was a musical composition performed for the first time?
  -  In which buildings was a musical composition performed for the first time?
- Which performers (musicians, singers) have performed a musical composition?
  - Which performers (musicians, singers) have performed a musical composition for the first time?

## Imported ontology modules
The Musical Performance ontology module imports the following modules:
- [Core](https://github.com/polifonia-project/core/)
- [Musical Composition](https://github.com/polifonia-project/musical-composition/)
